package ar.edu.unaj.apjw.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class EmployeeService {

    private final String URL = "http://localhost:9090/employees";

    private final ObjectMapper objectMapper = new ObjectMapper();

    private HttpClient httpClient = HttpClients.createDefault();

    public String findAll() throws IOException {
        HttpGet request = new HttpGet(URL);
        HttpResponse response = httpClient.execute(request);
        String result = EntityUtils.toString(response.getEntity());
        return result;
    }

    public String save(EmployeeDto newEmployeeDto) throws IOException {
        HttpPost request = new HttpPost(URL);
        request.addHeader("content-type", "application/json");
        request.setEntity(new StringEntity(objectMapper.writeValueAsString(newEmployeeDto)));
        HttpResponse response = httpClient.execute(request);
        String result = EntityUtils.toString(response.getEntity());
        return result;
    }

    public String findByName(String name) throws IOException {
        HttpGet request = new HttpGet(URL+"/"+name);
        HttpResponse response = httpClient.execute(request);
        String result = EntityUtils.toString(response.getEntity());
        return result;
    }

}
