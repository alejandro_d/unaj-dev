package ar.edu.unaj.apjw.client;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
class EmployeeDto {

    private String name;
    private String role;
    private Integer status;
}