package ar.edu.unaj.apjw.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApjwApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApjwApplication.class, args);
	}

}
