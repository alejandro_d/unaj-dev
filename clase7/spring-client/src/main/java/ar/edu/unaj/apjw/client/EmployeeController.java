package ar.edu.unaj.apjw.client;

import org.apache.http.HttpEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    // Aggregate root
    @GetMapping("/example")
    String all() throws IOException {
        return employeeService.findAll();
    }

    @PostMapping("/example")
    String newEmployee(@RequestBody EmployeeDto newEmployeeDto) throws IOException {
        return employeeService.save(newEmployeeDto);
    }

    @GetMapping("/example/{name}")
    String one(@PathVariable String name) throws IOException {
        return employeeService.findByName(name);
    }

}
