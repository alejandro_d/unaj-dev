package ar.edu.unaj.apjw;

public class EmployeeNotFoundException extends RuntimeException {

    EmployeeNotFoundException(String name) {
        super("Could not find employee " + name);
    }
}