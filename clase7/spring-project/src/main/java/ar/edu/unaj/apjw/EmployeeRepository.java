package ar.edu.unaj.apjw;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
interface EmployeeRepository extends JpaRepository<Employee, Long> {

    Employee findByName(String name);

    @Query("SELECT e FROM Employee e WHERE e.status = 1")
    Collection<Employee> findAllActiveEmployee();
}