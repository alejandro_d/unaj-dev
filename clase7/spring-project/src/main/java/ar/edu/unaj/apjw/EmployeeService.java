package ar.edu.unaj.apjw;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private final EmployeeRepository repository;

    @Autowired
    EmployeeService(EmployeeRepository employeeRepository){
        this.repository = employeeRepository;
    }

    public List<Employee> findAll() {
        return repository.findAll();
    }


    public Employee save(Employee newEmployee) {
        return repository.save(newEmployee);
    }

    public <T> Employee findByName(String name) throws EmployeeNotFoundException {
        return repository.findByName(name);
    }



    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    public Employee replaceEmployee(Employee newEmployee, Long id) {
        return repository.findById(id)
                .map(employee -> {
                    employee.setName(newEmployee.getName());
                    employee.setRole(newEmployee.getRole());
                    return repository.save(employee);
                })
                .orElseGet(() -> {
                    newEmployee.setId(id);
                    return repository.save(newEmployee);
                });
    }
}
