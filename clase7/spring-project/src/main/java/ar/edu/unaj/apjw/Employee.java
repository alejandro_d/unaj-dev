package ar.edu.unaj.apjw;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
class Employee {

    private @Id @GeneratedValue Long id;
    private String name;
    private String role;
    private Integer status;

    public Employee() {}

    public Employee(String name, String role,Integer status) {
        this.name = name;
        this.role = role;
        this.status = status;
    }

}