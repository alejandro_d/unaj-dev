package com.example.csvtojson.demo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.PostConstruct;
import java.io.*;
import java.time.Duration;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Slf4j
@Service
public class Test2Flux {

    private ObjectMapper objectMapper = new ObjectMapper();

    private final CsvSchema bootstrap = CsvSchema.emptySchema().withHeader().withColumnSeparator(',');
    private final CsvMapper csvMapper = new CsvMapper();

    @PostConstruct
    public void init(){
        String fileName = "data.csv";
        InputStream in = this.getClass().getResourceAsStream("/data.csv");
        try {
            getObjectFromFile(in)
                    .doOnNext(x -> System.out.println("1"+x.getId()))
                    .distinct(mc -> mc.getId())
                    .doOnNext(x -> System.out.println("2"+x.getId()))
                    .subscribe();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Flux<MessageCsvData> getObjectFromFile(InputStream inputStream) {
        log.debug("Received object to process : {}", "s3OS");
        AtomicInteger sequence = new AtomicInteger();
        return Mono.just(writeAsJson2(readObjectsFromCsv(inputStream))).flatMapMany((Flux::fromIterable))
                .flatMap(xlist -> createMessageCsvData("bucketName", "s3OS", sequence.getAndIncrement(), xlist));
    }

    private Flux<MessageCsvData> createMessageCsvData(String bucketName, String key, int andIncrement, List<String> l) {
        return Flux.just(new MessageCsvData(bucketName, key, andIncrement, l));
    }

    private MappingIterator<Map<?, ?>> readObjectsFromCsv(InputStream inputStream) {
        try {
            return csvMapper.readerFor(Map.class)
                    .with(bootstrap)
                    .readValues(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private List<List<String>> writeAsJson2(MappingIterator<Map<?, ?>> dataIterator) {
        if(dataIterator == null) return new ArrayList<>();
        return dataIteratorGetElements(dataIterator).stream().collect(new GroupingCollector<>(3));
    }

    private List<String> dataIteratorGetElements(MappingIterator<Map<?,?>> dataIterator) {
        List<String> values = new LinkedList<>();
        while (dataIterator.hasNext()){
            try {
                 values.add(getString((Map<Object, Object>) dataIterator.nextValue()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return values;
    }

    private String getString(Map<Object, Object> d) {
        try {
            d.forEach((x,y)-> d.put(x,y.toString()));
            String json = objectMapper.writeValueAsString(d);
            log.debug(" Convert to json : {}", json);
            return json;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "";
    }
}

