package com.example.csvtojson.demo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MessageCsvData {

    private String bucket;
    private String id;
    private int part;
    private List<String> documentList;
}


/*

 */