package ar.edu.unaj.apjw.hibernate;

import ar.edu.unaj.apjw.hibernate.entity.Employee;

import javax.persistence.EntityManager;

public class MainApp {

  public static void main(String[] args) {
    EntityManager entityManager = JPAUtil.getEntityManagerFactory().createEntityManager();
    entityManager.getTransaction().begin();

    Employee employee = new Employee("Ana","nothing",10200);
    entityManager.persist(employee);
    // Check database version
    String sql = "select * from Employee";

    Object[] r0 = (Object[]) entityManager.createNativeQuery(sql).getSingleResult();
    System.out.println(String.format("id : %s name: %s  designation: %s salary: %f", r0[0], r0[2], r0[1], r0[3] ));

    entityManager.getTransaction().commit();
    entityManager.close();

    JPAUtil.shutdown();
  }

}
