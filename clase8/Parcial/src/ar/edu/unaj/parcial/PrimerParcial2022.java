package ar.edu.unaj.parcial;

import java.util.PrimitiveIterator;
import java.util.Random;

class Consumer extends Thread{

    private Buffer buffer;
    NumbersGenerator numbersGenerator;
    String name;

    Consumer(String name, Buffer buffer, NumbersGenerator numbersGenerator) {
        this.buffer = buffer;
        this.numbersGenerator = numbersGenerator;
        this.name = name;
    }

    public void run() {
        PrimitiveIterator.OfInt randomIterator = new Random().ints(0, 2000 + 1).iterator();
        while(numbersGenerator.hasNext() && !buffer.bufferIsEmmpty()){
            try {
                int i = buffer.getValue();
                if (i > 0 ) System.out.println("Cunsumer name: "+name+" value : " + i);
                else {
                    System.out.println("Cunsumer name: " + name + " failed when try to recover a value");
                    while (i < 0) {
                        Thread.sleep(randomIterator.next());
                        i = buffer.getValue();
                        if (i > 0) System.out.println("Cunsumer name: " + name + " value : " + i);
                    }
                }
                Thread.sleep(randomIterator.next());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}

class Producer extends Thread {

    Buffer buffer;
    NumbersGenerator numbersGenerator;
    String name;

    Producer(String name, Buffer buffer, NumbersGenerator numbersGenerator) {
        this.buffer = buffer;
        this.numbersGenerator = numbersGenerator;
        this.name = name;
    }

    public void run() {
        PrimitiveIterator.OfInt randomIterator = new Random().ints(0, 2000 + 1).iterator();
        while(numbersGenerator.hasNext()){
            try {
                int numberGenerated = numbersGenerator.getValue();
                int i = buffer.putValue(numberGenerated);
                while( i < 0 ) {
                    Thread.sleep(randomIterator.next());
                    System.out.println("Cunsumer name: " + name + " failed when try to recover a value");
                    i = buffer.putValue(numberGenerated);
                }
                System.out.println("Producer name: " + name + " with value :" + numberGenerated);
                Thread.sleep(randomIterator.next());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

public class PrimerParcial2022{

    public static void main(String args[]){

        Buffer buffer = new Buffer();
        NumbersGenerator numbersGenerator = new NumbersGenerator();

        Producer t1 = new Producer("1",buffer,numbersGenerator);
        Consumer t2 = new Consumer("1",buffer,numbersGenerator);
        Consumer t3 = new Consumer("2",buffer,numbersGenerator);
        Producer t4 = new Producer("2",buffer,numbersGenerator);
        Consumer t5 = new Consumer("3",buffer,numbersGenerator);
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
    }
}

class Buffer{

    Integer[] buffer = new Integer[6];
    int i = 0;
    int j = 0;

    public Buffer(){
        for(int ii=0; ii<6; ii++) buffer[ii]=0;
    }

    public synchronized  int putValue(int val){
        if(bufferIsEmmpty()) return -1;
        buffer[i] = val;
        i++;
        if(i==6) i = 0;
        return 1;
    }

    public synchronized int getValue(){
        if(buffer[j]==0) return -1;
        int result = buffer[j];
        buffer[j] = 0;
        j++;
        if(j==6) j = 0;
        return result;
    }

    public boolean bufferIsEmmpty(){
        return false;
    }
}

class NumbersGenerator{

    int value = 0;

    public synchronized  int getValue(){
        if (value > 30) return -1;
        return value++;
    }

    public boolean hasNext(){
        if (value > 30) return false;
        return true;
    }
}