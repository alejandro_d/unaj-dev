package ar.edu.unaj.parcial;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class TableElect{

    private int mesa;

    private long dni;

    public TableElect(int mesa, long dni) {
        this.mesa = mesa;
        this.dni = dni;
    }

    public long getDni() {
        return dni;
    }

    public int getMesa(){
        return mesa;
    }
}

class Table {

    private List<TableElect> list;

    public Table(List<TableElect> list) {
        this.list = list;
    }

    public List<TableElect> getDataToPrint(int nmesa, String nm) {
        List<TableElect> collected = new ArrayList();
        synchronized (this) {//synchronized block
            Iterator i = list.iterator();
            while (i.hasNext()) {
                TableElect tableElect = (TableElect) i.next();
                if (tableElect.getMesa() == nmesa) {
                    collected.add(tableElect);
                }
            }
            list.removeAll(collected);
        }//end of the method syncronized
        return collected;
    }

}

class MyThread extends Thread {

    Table table;
    int value;
    String name;

    MyThread(Table table, int value, String name) {
        this.table = table;
        this.name = name;
        this.value = value;
    }

    public void run() {
        List<TableElect> result = table.getDataToPrint(value, name);
        System.out.println("End " + name + " \r\n");
        result.stream().forEach(c-> System.out.println(c.getDni()));
    }
}

public class PrimerParcial2019{

    public static void main(String args[]){

        List<TableElect> list = new ArrayList();
        list.add(new TableElect(123, 34321234L));
        list.add(new TableElect(127, 34526234L));
        list.add(new TableElect(121, 24321234L));
        list.add(new TableElect(123, 24321234L));
        list.add(new TableElect(123, 39321234L));
        list.add(new TableElect(127, 14526234L));

        Table obj = new Table(list);//only one object
        MyThread t1 = new MyThread( obj, 123, "Bob");
        MyThread t2 = new MyThread( obj, 123, "Alice");
        MyThread t3 = new MyThread( obj, 127, "George");
        MyThread t4 = new MyThread( obj, 123, "Glen");
        t1.start();
        t2.start();
        t3.start();
        t4.start();
    }
}