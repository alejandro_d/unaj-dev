package com.auth.client.model;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CustomerRegistrationRequest {

    private String userId;

    private String userName;

    private String email;

    private String mobile;

    private String address;

    private String password;
}
