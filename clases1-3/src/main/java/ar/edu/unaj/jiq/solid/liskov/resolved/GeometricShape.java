package ar.edu.unaj.jiq.solid.liskov.resolved;

public interface GeometricShape {

    double area();

    double perimeter();
}
