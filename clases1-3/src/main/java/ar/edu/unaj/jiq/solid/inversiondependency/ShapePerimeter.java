package ar.edu.unaj.jiq.solid.inversiondependency;

public interface ShapePerimeter {

    double perimeter();

}
