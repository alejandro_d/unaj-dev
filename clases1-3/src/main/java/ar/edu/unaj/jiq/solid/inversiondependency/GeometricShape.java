package ar.edu.unaj.jiq.solid.inversiondependency;

public interface GeometricShape extends ShapeArea, ShapePerimeter {

}
