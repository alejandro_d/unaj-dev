package ar.edu.unaj.jiq.solid.interfacesegregate;

public interface GeometricShape {

    double area();

    double perimeter();
}
