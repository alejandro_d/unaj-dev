package ar.edu.unaj.jiq.solid.inversiondependency.resolved;

public class AreaOperation {

    public static double area(ShapeArea shape){
        return shape.area();
    }

}
