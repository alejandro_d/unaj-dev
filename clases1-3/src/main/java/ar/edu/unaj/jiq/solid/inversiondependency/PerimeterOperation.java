package ar.edu.unaj.jiq.solid.inversiondependency;

public class PerimeterOperation {

    public static double perimeter(ShapePerimeter shape){
        return shape.perimeter();
    }

}
