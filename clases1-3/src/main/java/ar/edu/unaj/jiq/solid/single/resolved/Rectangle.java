package ar.edu.unaj.jiq.solid.single.resolved;

import java.util.List;

public class Rectangle {

    private int width;
    private int height;

    public Rectangle(int x, int y) {
        height = y;
        width = x;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
