package ar.edu.unaj.jiq.solid.inversiondependency.resolved;

public interface ShapeArea {

    double area();
}
