package ar.edu.unaj.jiq.solid.interfacesegregate.resolved;

public interface GeometricShape extends ShapeArea, ShapePerimeter {

}
