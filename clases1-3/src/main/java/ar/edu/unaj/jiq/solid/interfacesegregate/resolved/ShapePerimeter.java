package ar.edu.unaj.jiq.solid.interfacesegregate.resolved;

public interface ShapePerimeter {

    double perimeter();

}
