package ar.edu.unaj.jiq.solid.inversiondependency;

public interface ShapeArea {

    double area();
}
