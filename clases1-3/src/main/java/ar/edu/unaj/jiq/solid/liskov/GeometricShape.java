package ar.edu.unaj.jiq.solid.liskov;

public interface GeometricShape {

    double area();

    double perimeter();
}
