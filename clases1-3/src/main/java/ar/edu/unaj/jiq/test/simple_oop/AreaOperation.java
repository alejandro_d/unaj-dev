package ar.edu.unaj.jiq.test.simple_oop;

public class AreaOperation {

    public static double area(ShapeArea shape){
        return shape.area();
    }

}
