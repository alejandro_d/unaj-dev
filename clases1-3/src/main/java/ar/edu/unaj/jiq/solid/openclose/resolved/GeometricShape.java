package ar.edu.unaj.jiq.solid.openclose.resolved;

public interface GeometricShape {

    double area();

    double perimeter();
}
