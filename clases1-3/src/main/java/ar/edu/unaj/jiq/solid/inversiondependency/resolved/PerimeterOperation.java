package ar.edu.unaj.jiq.solid.inversiondependency.resolved;

public class PerimeterOperation {

    public static double perimeter(ShapePerimeter shape){
        return shape.perimeter();
    }

}
