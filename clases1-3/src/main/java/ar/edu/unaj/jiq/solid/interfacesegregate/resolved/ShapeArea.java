package ar.edu.unaj.jiq.solid.interfacesegregate.resolved;

public interface ShapeArea {

    double area();
}
