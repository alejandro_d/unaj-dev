package ar.edu.unaj.jiq.solid.inversiondependency.resolved;

public interface ShapePerimeter {

    double perimeter();

}
