package ar.edu.unaj.jiq.solid.inversiondependency.resolved;

public interface GeometricShape extends ShapeArea, ShapePerimeter {

}
